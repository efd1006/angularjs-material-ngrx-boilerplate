import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManagerAuthGuard } from './guards/manager.guard';

const routes: Routes = [
  {path: '', redirectTo: "/login",  pathMatch: 'full'},
  {path:"login", loadChildren: './modules/public/login/login.module#LoginModule'},
  {path:"register", loadChildren: './modules/public/register/register.module#RegisterModule'},
  {path: "dashboard", loadChildren: './modules/private/private.module#PrivateModule', canActivate: [ManagerAuthGuard]},
  {path: "**", redirectTo:"/login"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
