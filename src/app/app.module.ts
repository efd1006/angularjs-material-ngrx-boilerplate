import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { Router, Event, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';
import { AppComponent } from './app.component';
// import { LoginComponent } from './modules/public/login/login.component';
import { CoreModule } from './core/core.module';
import { UIService } from './core/services/ui.service';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools'
import { AppReducers, AppEffects, clearState } from './store/state/app.state';
import { AuthService } from './services/auth.service';
import { BaseService } from './services/base.service';
import { SessionService } from './services/session.service';
import { HttpClientModule } from '@angular/common/http';
import { TalentService } from './services/talent.service';

@NgModule({
  declarations: [
    AppComponent,
    
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    SharedModule,
    AppRoutingModule,
    CoreModule,
    /// store
    StoreModule.forRoot(AppReducers, {metaReducers: [clearState]}),
    EffectsModule.forRoot(AppEffects),
    StoreDevtoolsModule.instrument(),
  ],
  providers: [
    Title,
    AuthService,
    BaseService,
    SessionService,
    TalentService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor(private router: Router, private uiService:UIService){
    router.events.subscribe((event: Event)=>{

      if(event instanceof NavigationStart ){
        this.uiService.showLoader();
      }

      if (event instanceof NavigationEnd) {
        this.uiService.hideLoader();
      }

      if (event instanceof NavigationError) {
        this.uiService.hideLoader();
      }

    });
  }




}
