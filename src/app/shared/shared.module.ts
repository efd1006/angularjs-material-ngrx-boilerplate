import { NgModule } from '@angular/core';

import { MaterialModule } from './material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LoaderComponent } from './components/loader/loader.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';


import { BlueButtonComponent } from "./components/buttons/blue-button/blue-button.component"

@NgModule({
    declarations: [LoaderComponent, BlueButtonComponent],
    imports: [MaterialModule, FlexLayoutModule, CommonModule, FormsModule, NgxSkeletonLoaderModule],
    exports: [MaterialModule, FlexLayoutModule, LoaderComponent, BlueButtonComponent, CommonModule, FormsModule, NgxSkeletonLoaderModule]
})
export class SharedModule {}