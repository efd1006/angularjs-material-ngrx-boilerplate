import { Injectable } from "@angular/core";
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { SessionService } from './session.service';

@Injectable()
export class TalentService extends BaseService {
  constructor(
    public http: HttpClient,
    public sessionService: SessionService
  ) {
    super(http, sessionService)
  }

  loadTalents() {
    return this.$post(`/talent/all`, {})
  } 
}