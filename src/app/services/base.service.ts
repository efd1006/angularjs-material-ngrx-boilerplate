import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SessionService } from './session.service';
import { map } from 'rxjs/operators'
@Injectable()
export class BaseService {

  withToken: boolean = true;
  //public baseUrl = 'http://192.168.100.169:3000/api';
  public baseUrl = 'http://localhost:3000/api';

 
  constructor(
    public http: HttpClient,
    public sessionService: SessionService
  ) { }

  private getHeaders () {
    const headers = new HttpHeaders().append('Content-Type', 'application/json');
    // const token = this.sessionService.getSessionAuthToken();
    if (this.sessionService.getSessionAuthToken() && this.withToken) {
      console.log("Session :" + this.sessionService.getSessionAuthToken());
      const headers = new HttpHeaders().append('Content-Type', 'application/json')
        .append('Authorization',  'Bearer ' + this.sessionService.getSessionAuthToken());
      return headers;
    }

    return headers;
  }

  public $get(url: string, withToken: boolean = true) {
    this.withToken = withToken;
    console.log("URL: " + this.baseUrl + url);
    return this.http.get(this.baseUrl + url, { headers: this.getHeaders() });
  }

  public $post(url: string, data: any, withToken: boolean = true) {
    this.withToken = withToken;
    return this.http.post(this.baseUrl + url, JSON.stringify(data), { headers: this.getHeaders() });
  }

  public $put(url: string, data: any, withToken: boolean = true) {
    this.withToken = withToken;
    return this.http.put(this.baseUrl + url, JSON.stringify(data), { headers: this.getHeaders() });
  }

  public $delete(url: string, withToken: boolean = true) {
    this.withToken = withToken;
    return this.http.delete(this.baseUrl + url, { headers: this.getHeaders() });
  }


  public formDataPost(url: string, formData: FormData, withToken: boolean) {
    this.withToken = withToken;

    const headers = new HttpHeaders().append('Authorization', 'Bearer ' + this.sessionService.getSessionAuthToken());

    return this.http
      .post(this.baseUrl + url, formData, { headers: headers })
      .pipe(map(() => { return true; }))
  }
}