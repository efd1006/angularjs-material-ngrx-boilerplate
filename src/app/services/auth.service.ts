import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators'
import { SessionService } from './session.service';
import { IUser } from '../interfaces/user.interface';

@Injectable()
export class AuthService extends BaseService {

  constructor(
    public http: HttpClient, 
    public sessionService: SessionService,
  ) {
    super(http, sessionService);
  }

  signIn(email: string, password: string) {
    return this.$post(`/auth/login`, { email: email, password: password }, false)
      // .pipe(map(res => {
      //     this.dataStoreService.setBlockchainAddress(res['blockchain_address'])
      //     return res
      //     // this.sessionService.setSessionAuthToken(res['token'])
      //     // // this.sessionService.setSessionUserId(res['userId'])
      //   })
      // )
  }

  getCurrentUserId() {
    this.sessionService.getSessionUserId();
  }

  isLoggedIn() {
    const auth =  this.sessionService.getSessionAuthToken();
    return auth;
  }

  getAuthToken () {
      return this.sessionService.getAuthToken();
  }

  

  register (user: IUser) {
    return this.$post(`/auth/register`, user, false)
        
  }

  currentUser () {
    return this.$get(`/auth/me`)
  }

  logout() {
    this.sessionService.clearAll();
  }

}