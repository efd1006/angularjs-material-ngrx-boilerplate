import { Injectable } from '@angular/core';

@Injectable()
export class SessionService { 

  constructor() {}

  private sessionAuthToken: string = "";
  private sessionUserId: string = "";

  setSessionAuthToken(auth_token) {
    this.sessionAuthToken = String(auth_token)
    localStorage.setItem("auth_token", String(auth_token))
  }

  getSessionAuthToken() {
    if(this.sessionAuthToken) return this.sessionAuthToken;
    return localStorage.getItem("auth_token")
  }

  getAuthToken() {
    return localStorage.getItem("auth_token")
  }

  setSessionUserId(userId) {
    this.sessionUserId = String(userId);
    localStorage.setItem("user_id", String(userId))
  }

  getSessionUserId() {
    if(this.sessionUserId) return this.sessionUserId;

    return localStorage.getItem("user_id")
  }

  clearAll() {
    localStorage.clear()
    this.sessionAuthToken = null;
  }
}
