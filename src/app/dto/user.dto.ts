export class User {
  email: string
  password: string
  confirm_password?: string
  first_name: string
  last_name: string
  gender: string
  role: string
}