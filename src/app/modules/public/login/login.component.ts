import { Component, OnDestroy } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AppState } from 'src/app/store/state/app.state';
import { Store } from '@ngrx/store';
import { login } from 'src/app/store/actions/auth.action';
import { User } from 'src/app/dto/user.dto';
import { Observable, Subscription } from 'rxjs';
import * as fromAuthStore from '../../../store/selectors/auth.selector'
import { map, take } from 'rxjs/operators';
import { SessionService } from 'src/app/services/session.service';

export class ApiError {
  email: String[]
  password: String[]
}


@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnDestroy {

  private errorSubscription$: Subscription

  user: User
  loading: Observable<boolean>
  message: string
  errors = {
    email: [],
    password: []
  }

  constructor
  (
    private titleService: Title,
    private store: Store<AppState>,
  ) 
  {
    this.user = new User
    this.errors = new ApiError
    this.titleService.setTitle("Login");
    this.loading = this.store.select(fromAuthStore.selectAuthLoading)
    this.watchErrors()
  }

  ngOnDestroy() {
    this.errorSubscription$.unsubscribe()
  }

  ngOnInit() {
    this.errors = new ApiError
  }

  login() {
    let payload = {
      email: this.user.email,
      password: this.user.password
    }
    this.store.dispatch(login({ payload }))
  }

  watchErrors() {
    this.errorSubscription$ = this.store.select(fromAuthStore.selectAuthError).subscribe(error => {
      if (error) {
        if (typeof error == 'object') {
          for(let i = 0; i < error.errors.length; i++) {
            switch(error.errors[i].property) {
              case 'email':
                this.errors.email = Object.values(error.errors[i].constraints)
              break
              case 'password':
                  this.errors.password = Object.values(error.errors[i].constraints)
              break
            }
          } 
        } else {
          this.message = error
          this.errors = new ApiError
        }
      }
    })
  }

}