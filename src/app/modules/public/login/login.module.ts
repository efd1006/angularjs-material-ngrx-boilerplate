import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login.component'
import { SharedModule } from 'src/app/shared/shared.module';
import { Store, StoreModule } from '@ngrx/store';

const routes:Routes = [
    {path: "", component: LoginComponent}
];

@NgModule({
    declarations: [LoginComponent],
    imports: [RouterModule.forChild(routes), SharedModule, StoreModule],
    exports: [],
    providers: [
        Store
    ]
})
export class LoginModule{
}