
import { Component, OnDestroy } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { User } from 'src/app/dto/user.dto';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/state/app.state';
import { register } from 'src/app/store/actions/auth.action';
import { Observable, Subscription } from 'rxjs';
import * as fromAuthStore from '../../../store/selectors/auth.selector'

class ApiError {
  first_name: String[]
  last_name: String[]
  email: String[]
  password: String[]
  gender: String[]
  role: String[]
}

@Component({
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})

export class RegisterComponent implements OnDestroy {
  
  private errorSubscription$: Subscription
  user: User
  loading: Observable<boolean>
  message: string
  errors: ApiError

  constructor
  (
    private titleService: Title,
    private store: Store<AppState>
  ) 
  {
    this.user = new User
    this.errors = new ApiError
    this.titleService.setTitle("Register");
    this.loading = this.store.select(fromAuthStore.selectAuthLoading)
    this.watchErrors()
  }

  ngOnDestroy() {
    this.errorSubscription$.unsubscribe()
  }
  
  ngOnInit() {
    this.errors = new ApiError
  }

  register() {
    const payload = {
      user: this.user
    }
    console.log(this.user)
    this.store.dispatch(register({payload}))
  }

  watchErrors() {
    this.errorSubscription$ = this.store.select(fromAuthStore.selectAuthError).subscribe(error => {
      if (error) {
        if (typeof error == 'object') {
          for(let i = 0; i < error.errors.length; i++) {
            switch(error.errors[i].property) {
              case 'first_name':
                this.errors.first_name = Object.values(error.errors[i].constraints)
              break
              case 'last_name':
                  this.errors.last_name = Object.values(error.errors[i].constraints)
              break
              case 'email':
                  this.errors.email = Object.values(error.errors[i].constraints)
              break
              case 'password':
                  this.errors.password = Object.values(error.errors[i].constraints)
              break
              case 'gender':
                  this.errors.gender = Object.values(error.errors[i].constraints)
              break
              case 'role':
                  this.errors.role = Object.values(error.errors[i].constraints)
              break
            }
          } 
        } else {
          this.message = error
          this.errors = new ApiError
        }
      }
    })
  }
  
}