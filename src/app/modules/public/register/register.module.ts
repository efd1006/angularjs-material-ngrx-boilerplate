import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './register.component'
import { SharedModule } from 'src/app/shared/shared.module';
import { Store, StoreModule } from '@ngrx/store';

const routes:Routes = [
  {path: "", component: RegisterComponent}
];

@NgModule({
  declarations: [RegisterComponent],
  imports: [RouterModule.forChild(routes), SharedModule, StoreModule],
  exports: [],
  providers: [
      Store
  ]
})

export class RegisterModule{}