import { Component, ViewChild, OnDestroy } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/state/app.state';
import { loadTalents } from 'src/app/store/actions/talent.action';
import { ITalent } from 'src/app/interfaces/talent.interface';
import * as fromTalentStore from '../../../../store/selectors/talent.selector'
import { Subscription, Subject, Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
// const ELEMENT_DATA = [
//   { id: 1, firstName: "Ebeneser", lastName: "Roll", email: "eroll0@dailymail.co.uk", gender: "Male", age: "23" },
//   { id: 2, firstName: "Leanor", lastName: "Osbidston", email: "losbidston1@sitemeter.com", gender: "Female", age: "21" },
//   { id: 3, firstName: "Falkner", lastName: "Rowan", email: "frowan2@acquirethisname.com", gender: "Male", age: "35" },
//   { id: 4, firstName: "Suzy", lastName: "Suzy", email: "sfiltness3@slate.com", gender: "Female", age: "18" },
//   { id: 5, firstName: "Maxi", lastName: "Devoy", email: "mdevoy4@mail.ru", gender: "Female", age: "25" },
//   { id: 6, firstName: "Benetta", lastName: "Tuma", email: "btuma5@wikia.com", gender: "Female", age: "47" },
//   { id: 7, firstName: "Emanuele", lastName: "Yurkov", email: "eyurkov6@tinypic.com", gender: "Male", age: "39" },
//   { id: 8, firstName: "Vannie", lastName: "Pena", email: "vpena7@skype.com", gender: "Female", age: "19" },
//   { id: 9, firstName: "Cristina", lastName: "O' Mulderrig", email: "comulderrig8@yahoo.co.jp", gender: "Female", age: "22" },
//   { id: 10, firstName: "Bartholemy", lastName: "Kubis", email: "bkubis9@nps.gov", gender: "Male", age: "26" }
// ];




@Component({
    selector: 'app-tables',
    templateUrl: './tables.component.html',
    styleUrls: ['./tables.component.scss'],
})
export class TablesComponent implements OnDestroy {
  private ngUnsubscribe = new Subject();
  talentsSubscription: Subscription
  talentLoading$: Observable<boolean>
  talentLoaded$: Observable<boolean>

  // displayedColumns: string[] = ['id', 'firstName', 'lastName', 'email', 'gender', 'age'];

  displayedColumnsAdvanced: string[] = ['first_name', 'last_name', 'alias', 'age', 'price', 'actions'];
  // dataSource = ELEMENT_DATA;

  tal = [
    { first_name: 'Ed', last_name: 'Mar', alias: 'ee', age: 1, price: 2 }
  ]

  dataSourceAdvancedTable
  
  @ViewChild(MatPaginator) paginator: MatPaginator;


  ngOnInit() {
  this.dataSourceAdvancedTable.paginator = this.paginator;
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next()
    this.ngUnsubscribe.complete()
  }

  public constructor(
    private titleService:Title,
    private store: Store<fromTalentStore.TalentState>
  ) {
    this.titleService.setTitle("Talents");
    this.store.dispatch(loadTalents())
    this.talentLoading$ = this.store.select(fromTalentStore.selectTalentLoading)
    this.talentsSubscription = this.store.select(fromTalentStore.selectTalent).pipe(takeUntil(this.ngUnsubscribe))
    .subscribe(data => {
      this.dataSourceAdvancedTable = new MatTableDataSource(data.talents);
    })
    this.talentLoaded$ = this.store.select(fromTalentStore.selectTalentLoaded)
    
  }

  applyFilter(filterValue: string) {
    this.dataSourceAdvancedTable.filter = filterValue.trim().toLowerCase();
  }

}