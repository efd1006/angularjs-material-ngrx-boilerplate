import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/state/app.state';
import { logout } from 'src/app/store/actions/auth.action';
import { Router } from '@angular/router';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {
    constructor(
        private store$: Store<AppState>,
        private router: Router
    ) {

    }

    logout() {
        this.store$.dispatch(logout())
        this.router.navigate(['/login'])
    }
}