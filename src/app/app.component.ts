import { Component } from '@angular/core';
import { AppState } from './store/state/app.state';
import { Store } from '@ngrx/store';
import { SessionService } from './services/session.service';
import { me } from './store/actions/auth.action';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-material-dashboard';
  constructor(
    private store: Store<AppState>,
    private sessionService: SessionService
  ) 
  {
    let token = this.sessionService.getSessionAuthToken()
    if(token) {
      this.store.dispatch(me())
    }
  }
}
