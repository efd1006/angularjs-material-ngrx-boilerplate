import { Injectable } from '@angular/core'
import { TalentService } from 'src/app/services/talent.service';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { loadTalents, loadTalentsSuccess, loadTalentsFailure } from '../actions/talent.action';
import { switchMap, map, catchError, tap, delay } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable()
export class TalentEffects {
  
  constructor(
    private talentService: TalentService,
    private actions$: Actions
  ) {}

  loadTalents$ = createEffect(() => 
    this.actions$.pipe(
      ofType(loadTalents),
      delay(1000),
      switchMap(action => {
        return this.talentService.loadTalents()
        .pipe(
          map((res: any) => loadTalentsSuccess({payload: { talents: res }})), 
          catchError((error:any) => of(loadTalentsFailure({payload: { error: error.error.message }})) ) 
        )
      })
    )
  )

  loadTalentSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadTalentsSuccess),
      tap(action => console.log(action))
    ),
    { dispatch: false }
  )

  loadTalentFailure$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadTalentsFailure),
      tap(action => console.log(action))
    ),
    { dispatch: false }
  )

}