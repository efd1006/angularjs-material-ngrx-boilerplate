import { Injectable } from "@angular/core";
import { createEffect, Actions, ofType } from '@ngrx/effects'
import { AuthService } from 'src/app/services/auth.service';
import { login, loginSuccess, loginFailure, register, registerSuccess, registerFailure, me, meSuccess, meFailure } from '../actions/auth.action'
import { map, catchError, tap, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { SessionService } from 'src/app/services/session.service';
import { Router } from '@angular/router';
@Injectable()
export class AuthEffects {

  constructor
  (
    private authService: AuthService,
    private actions$: Actions,
    private sessionService: SessionService,
    private router: Router
  )
  {

  }

  login$ = createEffect(() =>
    this.actions$.pipe(
      ofType(login),
      switchMap(action => {
        return this.authService.signIn(action.payload.email, action.payload.password)
        .pipe(
          map((res: any) => loginSuccess({payload: {user: res.user, token: res.token }})),
          catchError((error: any) =>  {
            return of(loginFailure({payload: {error: error.error.message}}))
          })
        )
      })
    ),
    { resubscribeOnError: false }
  )

  loginSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loginSuccess),
      tap(action => {
        this.sessionService.setSessionAuthToken(action.payload.token)
        this.sessionService.setSessionUserId(action.payload.user._id)
        this.router.navigate(['/dashboard'])
      })
    ), 
    {dispatch: false}
  )

  loginFailure$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loginFailure),
      tap(action => console.log(action))
    ), 
    {dispatch: false}
  )

  register$ = createEffect(() => 
    this.actions$.pipe(
      ofType(register),
      switchMap(action => {
        return this.authService.register(action.payload.user)
        .pipe(
          map((res: any) => registerSuccess({payload: { message: res.message }})),
          catchError((error) => of(loginFailure({payload: { error: error.error.message }})))
        )
      })
    )
  )

  registerSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(registerSuccess),
      tap(action => console.log(action))
    ),
    { dispatch: false }
  )

  registerFailure$ = createEffect(() =>
    this.actions$.pipe(
      ofType(registerFailure),
      tap(action => console.log(action))
    ),
    { dispatch: false }
  )

  me$ = createEffect(() => 
    this.actions$.pipe(
      ofType(me),
      switchMap(action => {
        return this.authService.currentUser()
        .pipe(
          map((res: any) => meSuccess({ payload: { user: res.user } })),
          catchError(error => of(meFailure({ payload: { error: error.error.message} })))
        )
      })
    )
  )

  meSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(meSuccess),
      tap(action => console.log(action))
    ),
    { dispatch: false }
  )

  meFailure$ = createEffect(() =>
    this.actions$.pipe(
      ofType(meFailure),
      tap(action => console.log(action))
    ),
    { dispatch: false }
  )

}