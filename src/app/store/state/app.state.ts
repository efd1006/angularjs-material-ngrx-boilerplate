import * as fromAuth from '../reducers/auth.reducer'
import * as fromTalent from '../reducers/talent.reducer'
import { ActionReducerMap, Action } from '@ngrx/store'
import { AuthEffects } from '../effects/auth.effect'
import { TalentEffects } from '../effects/talent.effect'
import { login, logout } from '../actions/auth.action'

export interface AppState {
  readonly auth: fromAuth.AuthState
  readonly talents: fromTalent.TalentState
}

export const AppReducers: ActionReducerMap<AppState> = {
  auth: fromAuth.reducer,
  talents: fromTalent.reducer
}

export const AppEffects = [
  AuthEffects,
  TalentEffects,
]

export function clearState(reducer) {
  return (state, action) => {
    if (action.type === logout.type) {
      state = undefined;
    }
    return reducer(state, action);
  };
}