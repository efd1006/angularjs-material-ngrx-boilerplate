import { createAction, props } from '@ngrx/store'
import { ITalent } from 'src/app/interfaces/talent.interface'

export const loadTalents = createAction(
  '[TALENTS] Load Talents'
)

export const loadTalentsSuccess = createAction(
  '[TALENTS] Load Talents Success',
  props<{ payload: { talents: ITalent[] }}>()
) 

export const loadTalentsFailure = createAction(
  '[TALENTS] Load Talents Failure',
  props<{ payload: { error: string }}>()
)

