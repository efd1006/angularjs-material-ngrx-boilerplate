import { createAction, props } from '@ngrx/store'
import { IUser } from 'src/app/interfaces/user.interface'

/**
 * LOGIN ACTIONS
 */
export const login = createAction(
  '[AUTH] Login',
  props<{ payload: { email: string, password: string }}>()
)

export const loginSuccess = createAction(
  '[AUTH] Login Success',
  props<{ payload: { user: IUser, token: string }}>()
)

export const loginFailure = createAction(
  '[AUTH] Login Failure',
  props<{ payload: { error: string }}>()
)

/**
 * REGISTER ACTIONS
 */
export const register = createAction(
  '[AUTH] Register',
  props<{ payload: {user: IUser}}>()
)

export const registerSuccess = createAction(
  '[AUTH] Register Success',
  props<{ payload: {message: string}}> ()
)

export const registerFailure = createAction(
  '[AUTH] Register Failure',
  props<{ payload: { error: string }}>()
)

/**
 * ME
 */
export const me = createAction(
  '[AUTH] ME'
)

export const meSuccess = createAction(
  '[AUTH] ME Success',
  props<{ payload: { user: IUser }}>()
)

export const meFailure = createAction(
  '[AUTH] ME Failure',
  props<{ payload: { error: string }}>()
)


export const logout = createAction(
  '[AUTH] Logout'
)