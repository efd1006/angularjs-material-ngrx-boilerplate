import { IUser } from 'src/app/interfaces/user.interface';
import { createReducer, on, Action } from '@ngrx/store';
import { login, loginSuccess, loginFailure, register, registerSuccess, registerFailure, me, meSuccess, meFailure } from '../actions/auth.action';

export interface AuthState {
  user: IUser,
  isAuthenticated: boolean,
  token: string,
  isLoading: boolean
  message: any | null,
} 

export const initialState: AuthState = {
  user: null,
  isAuthenticated: false,
  token: null,
  isLoading: false,
  message: null
}

const authReducer = createReducer(
  initialState,
  on(login, (state) => ({ 
    ...state, 
    isLoading: true
  })),
  on(loginSuccess, (state, { payload }) => ({ 
    ...state, 
    user: payload.user, 
    token: payload.token, 
    isAuthenticated: true, 
    isLoading: false,
    message: null
  })),
  on(loginFailure, (state, { payload }) => ({
    ...state,
    isAuthenticated: false,
    isLoading: false,
    message: payload.error
  })),
  on(register, (state) => ({ 
    ...state, 
    isLoading: true
  })),
  on(registerSuccess, (state, { payload }) => ({ 
    ...state, 
    isLoading: false, 
    message: payload.message 
  })),
  on(registerFailure, (state, {payload}) => ({ 
    ...state, 
    isLoading: false, 
    message: payload.error 
  })),
  on(me, (state) => ({ 
    ...state, 
    isLoading: true 
  })),
  on(meSuccess, (state, {payload}) => ({
    ...state, isLoading: 
    false, 
    isAuthenticated: true, 
    user: payload.user 
  })),
  on(meFailure, (state, {payload}) => ({ 
    ...state, 
    isLoading: false, 
    isAuthenticated: false, 
    user: null, 
    message: payload.error 
  }))
)

export function reducer(state: AuthState | undefined, action: Action) {
  return authReducer(state, action)
}