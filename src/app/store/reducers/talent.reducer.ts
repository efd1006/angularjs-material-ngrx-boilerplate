import { ITalent } from 'src/app/interfaces/talent.interface';
import { createReducer, on, Action } from '@ngrx/store';
import { loadTalents, loadTalentsSuccess, loadTalentsFailure } from '../actions/talent.action';


export interface TalentState {
  talents: ITalent[],
  isLoading: boolean,
  isLoaded: boolean,
  message: string | null
}

export const initialState: TalentState = {
  talents: null,
  isLoading: false,
  isLoaded: false,
  message: null
}

const talentReducer = createReducer(
  initialState,
  on(loadTalents, (state) => ({
    ...state,
    isLoading: true,
    isLoaded: false
  })),
  on(loadTalentsSuccess, (state, { payload }) => ({
    ...state,
    talents: payload.talents,
    isLoading: false,
    isLoaded: true,
    message: null
  })),
  on(loadTalentsFailure, (state, { payload }) => ({
    ...state,
    talents: null,
    isLoading: false,
    isLoaded: true,
    message: payload.error
  })),
)

export function reducer(state: TalentState | undefined, action: Action) {
  return talentReducer(state, action)
}