
import { createFeatureSelector, createSelector } from '@ngrx/store'
import { ITalent } from '../../interfaces/talent.interface'

export interface TalentState {
  talents: ITalent[],
  isLoading: boolean
  isLoaded: boolean
  message: any| null
}

export const selectTalent = createFeatureSelector<TalentState>('talents')

export const selectTalents = createSelector(
  selectTalent,
  (state: TalentState) => state.talents
)

export const selectTalentLoading = createSelector(
  selectTalent,
  (state: TalentState) => state.isLoading
)

export const selectTalentLoaded = createSelector(
  selectTalent,
  (state: TalentState) => state.isLoaded
)