import * as fromState from '../state/app.state'
import { createSelector, createFeatureSelector } from '@ngrx/store'
import { IUser } from '../../interfaces/user.interface'

export interface AuthState {
  user: IUser
  isLoading: boolean,
  role: string,
  message: any | null
}

export const selectAuth = createFeatureSelector<AuthState>('auth')

export const selectAuthUser = createSelector(
  selectAuth,
  (state: AuthState) => state.user
)

export const selectUserRole = createSelector(
  selectAuth,
  (state: AuthState) => state.user.role
)

export const selectAuthError = createSelector(
  selectAuth,
  (state: AuthState) => state.message
)

export const selectAuthLoading = createSelector(
  selectAuth,
  (state: AuthState) => state.isLoading
)