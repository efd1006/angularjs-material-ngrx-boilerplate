export interface IUser {
  _id?: number
  email: string
  readonly password: string
  first_name: string
  last_name: string
  gender: string
  role: string
  token?: string
  is_authenticated?: boolean
  loading?: boolean
}

export interface LoadUser {
  loading: boolean
  data: IUser
}