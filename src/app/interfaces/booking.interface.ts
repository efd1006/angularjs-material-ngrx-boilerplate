import { IUser } from './user.interface';
import { ITalent } from './talent.interface';

export interface IBooking {
  customer: IUser;
  talent: ITalent;
  start_date: Date;
  end_date: Date;
  price: Number;
  pops: String;
  chats: IChat[];
  status: String;
  created_at: Date

}
export interface IChat {
  sender: IUser;
  receiver: IUser;
  message: String;
  created_at: Date
}