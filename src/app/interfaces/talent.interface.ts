import { IUser } from './user.interface';
import { IBooking } from './booking.interface';

export interface ITalent {
  first_name: String;
  last_name: String;
  alias: String;
  age: Number;
  vital_statistics: IVitalStatistic;
  manager: IUser;
  price: Number;
  gallery: String[];
  reviews: IReview [];
  created_at: Date;
}

export interface IVitalStatistic {
  burst: Number;
  waist: Number;
  hip: Number;
}

export interface IReview {
  rating: Number;
  message: String;
  customer: IUser;
  booking: IBooking;
  created_at: Date;
}
