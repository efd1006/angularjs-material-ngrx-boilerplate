import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

import * as fromAuth from '../store/selectors/auth.selector'
import { SessionService } from '../services/session.service';

@Injectable({
  providedIn: 'root',
})
export class ManagerAuthGuard implements CanActivate {
  constructor(private sessionService: SessionService,private store: Store<fromAuth.AuthState>, private router: Router) {}

  canActivate() {
    let token = this.sessionService.getSessionAuthToken()
    if(token) {
      return this.store.pipe(
        select(fromAuth.selectUserRole),
        map(role => {
          if (role != 'MANAGER') {
            this.router.navigate(['/login'])
            return false;
          }
          return true;
        }),
        take(1)
      );
    }
    this.router.navigate(['/login'])
    return false
  }
}